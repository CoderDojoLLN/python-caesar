def decrypt(char, key):
    '''Déchiffre le caractère `char` avec la clé `key`
    '''
    # Calcule le décalage de la clé
    shift = ord(key)-ord('a')
    # Convertit le caractère en nombre
    num = ord(char)
    # Il ne faut déchiffrer le caractère
    # que si c'est une miniscule ou une majuscule
    # Cas 1: Le caractère est une minuscule
    if ord('a') <= num <= ord('z'):
        num -= shift
        # Si le caractère déchiffré est avant `a`, il faut ajouter 26
        if num < ord('a'):
            num+=26
    # Cas 2: Le caractère est une majuscule
    elif ord('A') <= num <= ord('Z'):
        num -= shift
        # Si le caractère chiffré est avant `A`, il faut ajouter 26
        if num < ord('A'):
            num+=26
    return chr(num)

# Ouvrir le fichier avec le texte à déchiffer
with open('text_secret.txt') as file_d:
    encrypted_text = file_d.read()

# Clé utilisée pour chiffrer
key = 'c'

# Lire tous les caractères du texte afin de les déchiffrer 1 par 1
text = ''
for char in encrypted_text:
    text += decrypt(char, key)

# Ouvrir un fichier et enregistrer le résultat
with open('text_decrypted.txt', 'w') as file_d:
    file_d.write(text)

