def encrypt(char, key):
    '''Chiffre le caractère `char` avec la clé `key`
    '''
    # Calcule le décalage
    shift = ord(key)-ord('a')
    # Convertit le caractère en nombre
    num = ord(char)
    # Il ne faut convertir le caractère
    # que si c'est une miniscule ou une majuscule
    # Cas 1: Le caractère est une minuscule
    if ord('a') <= num <= ord('z'):
        num += shift
        # Si le caractère chiffré est après `z`, il faut retirer 26
        if num > ord('z'):
            num-=26
    # Cas 2: Le caractère est une majuscule
    elif ord('A') <= num <= ord('Z'):
        num += shift
        # Si le caractère chiffré est après `Z`, il faut retirer 26
        if num > ord('Z'):
            num-=26
    return chr(num)

# Ouvrir le fichier avec le texte à chiffer
with open('text.txt') as file_d:
    text = file_d.read()

# Clé utilisée pour chiffrer
key = 'c'

# Lire tous les caractères du texte afin de les chiffrer 1 par 1
encrypted_text = ''
for char in text:
    encrypted_text += encrypt(char, key)

# Ouvrir un fichier et enregistrer le résultat
with open('text_secret.txt', 'w') as file_d:
    file_d.write(encrypted_text)

