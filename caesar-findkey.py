try:
    import matplotlib.pyplot as plt
    _matplotlib = True
except ImportError:
    import warnings
    warnings.warn("Matplotlib not found. Plot disable.", ImportWarning)
    _matplotlib = False

def show_count(count):
    '''Affiche un graphique avec les comptes des lettres
    '''
    # Si matplotlib n'est pas installé, la fonction ne fait rien
    if _matplotlib:
        plt.plot(range(0,26), count)
        plt.xlabel('Lettre')
        plt.ylabel('Compte')
        plt.xticks(range(0, 26), [chr(i) for i in range(ord('a'), ord('z'))])
        plt.show()

# Ouvrir le fichier chiffré
with open('text_secret.txt') as file_d:
    encrypted_text = file_d.read()

# Compte chacune des lettres
count = [0] * 26
for char in encrypted_text.lower():
    if ord('a') <= ord(char) <= ord('z'):
        count[ord(char)-ord('a')]+=1

# Trouve la lettre la plus représentée
# Cette lettre doit correspondre à la lettre déchiffrée
argmax = lambda x: count[x]
e_pos = max(range(0, 26), key=argmax)

# Sur base de la lettre correspondant au e,
# retrouve la clé
key = chr(ord('a') + ((e_pos - 4) % 26))
print(f'Après analyse du texte, la clé doit être: {key}')

show_count(count)
