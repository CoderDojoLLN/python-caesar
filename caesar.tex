\documentclass[12pt, hidelinks]{article}
\synctex=1
\usepackage{graphicx}
\usepackage[utf8x]{inputenc}
\usepackage[frenchb]{babel}
\usepackage[T1]{fontenc}
\usepackage[a4paper, top=2.5cm, bottom=2.5cm, inner=2cm, outer=5.5cm, marginparwidth=4cm, marginparsep=0.5cm]{geometry}
\usepackage[factor=1000]{microtype}     %awesome package to deal with text spacing, special char in margin...
\usepackage{lmodern}                    %best font for latex (latin modern is derived from computer modern)
\usepackage{amssymb,array}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{xspace}
\usepackage{color}
\usepackage{tikz}
\usepackage{listings}
\usepackage{upquote}
\usepackage{framed}
\usepackage{marginnote}
\usepackage{ragged2e}
\usepackage{hyperref}

\author{CoderDojo Louvain-la-Neuve}

\title{Introduction à la cryptographie avec César}
\date{Février 2018}

\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.7,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}
\definecolor{deepgray}{rgb}{0.4,0.4,0.4}

% Default fixed font does not support bold face
\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{12} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{12}  % for normal

\setlength{\parindent}{0em}
\setlength{\parskip}{0.5em}

\usetikzlibrary{calc}
\tikzset{>=latex}

\newcommand{\trad}[1]{\marginnote{\textcolor{deepblue}{#1}}}
\newcommand{\tradd}[1]{\marginpar{\textcolor{deepblue}{#1}}}

\newcounter{ccount}

\renewcommand{\familydefault}{\sfdefault}
\lstset{language=Python,
texcl=true,
basicstyle=\ttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
commentstyle=\ttb\color{deepgray},
showstringspaces=false,
%inputencoding=utf8,
extendedchars=true,            %
literate={è}{{\`e}}1 {é}{{\'e}}1,
escapeinside={(*@}{@*)},
}

\begin{document}

\maketitle

Pendant cette session,\trad{\RaggedRight Retrouve les traductions anglais~$\rightarrow$~français dans cette marge.}
nous allons découvrir la cryptographie avec le chiffrement de César.
Le chiffrement de César était utilisé par Jules César pour protéger ses messages militaires.

\section{Le chiffrement de César}

\label{explications}

Le chiffrement de César consiste à décaler les lettres de l'alphabet de une ou plusieurs positions.
Dans le cas ci-dessous, le A devient C, le B devient D, etc.
Pour les dernières lettres de l'alphabet, on recommence au début: le \textit{Y} devient \textit{A} et le \textit{Z} devient \textit{B}.

Dans l'exemple, comme le \textit{A} devient \textit{C}, on dit que la clé est \textit{C}.
À partir de \textit{C}, on peut déterminer toutes les correspondances entre les lettres.

\begin{center}
\begin{tikzpicture}[x=1cm,y=1cm,%
                    char/.style = {
                        rectangle, draw=black!50,
                        very thick,
                        minimum width=0.6cm,
                        minimum height=0.6cm,
                        },
                    ]
    
    \node[right] at (-4.5, 0) {Lettres avant chiffrement:};
    \node[right] at (-4.5, -2) {Lettres après chiffrement:};
    
    \foreach \i in {1,...,6}
    {
        \setcounter{ccount}{\i}
        \node[char] at (\i*0.6, 0) {\Alph{ccount}};
        \node[char] at (\i*0.6, -2) {\Alph{ccount}};
    };
    
    \foreach \i in {1,...,6}
    {
        \draw[->, very thick] (\i*0.6, -0.3) to [out=270, in=90] (\i*0.6+1.2, -1.7);
    };
    
    \node at (4.8, 0) {$\cdots$};
    \node at (4.8, -2) {$\cdots$};
    
    \foreach \i in {24,...,26}
    {
        \setcounter{ccount}{\i}
        \node[char] at ((\i*0.6 - 8.4, 0) {\Alph{ccount}};
        \node[char] at ((\i*0.6 - 8.4, -2) {\Alph{ccount}};
    };
    
    \foreach \i in {1,...,3}
    {
        \draw[->, very thick] (\i*0.6 + 4.2, -0.3) to [out=270, in=90] (\i*0.6+5.4, -1.7);
    };
    \draw[->, very thick] (6.6, -0.3)
        .. controls +(down:8mm) and +(up:6mm) .. (7.65, -1.7)
        .. controls +(down:6mm) and +(up:6mm) .. (7.65, -2.3)
        .. controls +(down:8mm) and +(right:10mm) .. (3.5, -3)
        .. controls +(left:10mm) and +(down:7mm) .. (0.6, -2.3);
    \draw[->, very thick] (7.2, -0.3)
        .. controls +(down:8mm) and +(up:6mm) .. (7.8, -1.7)
        .. controls +(down:6mm) and +(up:6mm) .. (7.8, -2.3)
        .. controls +(down:11mm) and +(right:10mm) .. (3.3, -3.15)
        .. controls +(left:10mm) and +(down:7mm) .. (1.2, -2.3);

\end{tikzpicture}
\end{center}

Si on chiffre la phrase « Super message secret » avec la clé \textit{C}, elle devient « Uwrgt oguucig ugetgv ».

Pour déchiffrer le message, il faut suivre les flèches en sens inverse.

\section{Chiffrer un caractère}

Avant de chiffrer une phrase complète, il faut pouvoir chiffrer un caractère.

Malheureusement, on ne peut pas faire de calculs avec des lettres.
C'est pourquoi on doit convertir les lettres en chiffres.
Python fournit deux fonctions qui seront bien utiles:
\begin{itemize}
    \item \lstinline{ord()} convertit un caractère en chiffre : « A » en 65, « B » en 66, « a » en 97, « b » en 98\dots
    \item \lstinline{chr()} convertit un chiffre en caractère : 65 en « A », 66 en « B », 97 en « a », 98 en « b »\dots
\end{itemize}

Créons une fonction qui prend comme argument une lettre et une clé et qui retourne une lettre chiffrée.

\begin{lstlisting}
def encrypt(char, key):(*@\tradd{encrypt~=~chiffrer\\char~=~diminutif de caracrère\\key~=~clé}@*)
    '''Chiffre le caractère `char` avec la clef `key`
    '''
    # Convertit le caractère en nombre
    num = ord(char)
    
    # Calcule le décallage lié à la clé
    offset = ord(key) - ord('a')(*@\tradd{offset~=~décalage}@*)
    
    # Effectue le décalage sur le chiffre
    num += offset # Équivalent à num = num - offset
    
    # Convertit le chiffre en caractère
    return chr(num)(*@\tradd{return~=~retourner}@*)
\end{lstlisting}

Pour chiffrer la lettre \lstinline{'b'} avec la clé \lstinline{'c'} à l'aide de \lstinline{encrypt}, il faut faire:
\begin{lstlisting}
encrypt('b', 'c')
\end{lstlisting}

Pour afficher le résultat dans la console, tu peux utiliser \lstinline{print}\trad{print~=~imprimer}:
\begin{lstlisting}
print(encrypt('b', 'c'))
\end{lstlisting}

Tu as chiffré ta première lettre. Est-ce bien le résultat attendu?

Mais que se passe-t-il si tu chiffres \lstinline{'y'} avec la clé \lstinline{'c'}?
Quel est le problème?
Regarde bien l'exemple au point \ref{explications}.
N'hésite pas à demander de l'aide aux mentors.

Dès que tu as trouvé l'erreur, corrige ton code pour résoudre le problème.
Pour corriger ton code, tu auras besoin d'utiliser des conditions.

Les conditions peuvent s'écrire de la façon suivante:

\begin{lstlisting}
if num > 122:(*@\tradd{if~=~si}@*)
    # Faire quelque chose ici
\end{lstlisting}

Les différentes conditions possibles sont:
\begin{itemize}
    \item Plus grand: \lstinline{>}
    \item Plus grand ou égal: \lstinline{>=}
    \item Égal: \lstinline{==}
    \item Plus petit ou égal: \lstinline{<=}
    \item Plus petit: \lstinline{<}
\end{itemize}

Les conditions peuvent être combinées en utilisant les mots clés \lstinline{and} et \lstinline{or}.\trad{and~=~et\\or~=~ou}

\section{Chiffrement des lettres seulement}

Dans la version actuelle, ta fonction convertit tous les caractères qu'elle reçoit.
On voudrait ne convertir que les majuscules et les minuscules afin de ne pas modifier les espaces et les signes de ponctuation.

Ajoute des conditions dans ta fonction pour n'effectuer le décalage que pour les caractères entre \lstinline{a} et \lstinline{z} et entre \lstinline{A} et \lstinline{Z}.
Laisse les autres caractères inchangés.

\section{Chiffrer une phrase}

Pour chiffrer une phrase, tu peux t'inspirer du code ci-dessous.
Dans ce code, tous les caractères de \lstinline{text} sont lus les uns après les autres.
Il sont convertis et ajouté à \lstinline{encrypted_text}\trad{encrypted text~=~texte chiffré}.

\begin{lstlisting}
text = 'Super message secret'
key = 'c'

encrypted_text = ''
for char in text:
    encrypted_text += encrypt(char, key)
\end{lstlisting}

Tu peux ensuite afficher le résultat de ton chiffrement en faisant
\begin{lstlisting}
print(encrypted_text)
\end{lstlisting}

Amuse-toi! Change le message, change la clé.

\section{Déchiffre un message chiffré}

Inspire-toi de ta fonction \lstinline{encrypt} afin de créer une nouvelle fonction \lstinline{decrypt}\trad{decrypt~=~déchiffer}. Cette fonction déchiffre un caractère à partir d'une clé.

Inspire-toi du reste de ton code pour pouvoir déchiffrer une phrase.

\section{Chiffre et déchiffre un fichier}

Les mentors ont un fichier que tu dois déchiffrer avec la clé \lstinline{'c'}.
Déchiffre-le et enregistre-le dans un nouveau fichier avec ton code.

Pour lire un fichier, tu dois faire
\begin{lstlisting}
# Ouvrir le fichier avec le texte à déchiffer
with open('text_secret.txt') as file_d:(*@\tradd{with~=~avec\\open~=~ouvrir\\as~=~en tant que\\file~=~fichier\\read~=~lire}@*)
    encrypted_text = file_d.read()
\end{lstlisting}

Le contenu du fichier chiffré se trouve dans la variable \lstinline{encrypted_text}.

Déchiffre \lstinline{encrypted_text} et sauve le résultat dans un nouveau fichier.
\begin{lstlisting}
# Ouvrir un fichier et enregistrer le résultat
with open('text_decrypted.txt', 'w') as file_d:
    file_d.write(text)(*@\tradd{write~=~écrire}@*)
\end{lstlisting}

\section{Bonus: déchiffrer sans la clé}

Le chiffrement de César n'est pas un chiffrement sécurisé.
En effet, le nombre de clés n'est pas élevé.
Il n'en existe que 26 différentes en comptant la clé \lstinline{'a'} qui ne modifie pas le message.
Il est donc possible de tester toutes les clés afin de trouver le message.

Mais il est également possible de retrouver la clé sans même les tester.
En effet, en français, certaines lettres sont plus utilisées que d'autres.
La lettre \textit{e} est la lettre la plus utilisée suivie des lettres \textit{a}, \textit{i} et \textit{s}.

On peut donc compter le nombre de fois où chaque lettre est utilisée dans le message chiffré.
La lettre la plus représentée devrait correspondre à la lettre \textit{e} dans le message déchiffré.
On peut donc déduire la clé sur base de la lettre la plus représentée dans le message.

À toi de jouer, essaye de déchiffrer sans la clé.
N'hésitez pas à travailler en équipe et à demander de l'aide aux mentors.

\end{document}

